﻿using Invasion.Classes.Simulation;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Invasion
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.ComboBoxMode.ItemsSource = Enum.GetValues(typeof(Mode));
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            int iterateNmb = 0;
            int soldierNmb = 0;
            int ufoNmb = 0;
            int mapSizeNmb = 0;

            try
            {
                iterateNmb = int.Parse(this.TextBoxIterateNmb.Text);
                soldierNmb = int.Parse(this.TextBoxSoldierNmb.Text);
                ufoNmb = int.Parse(this.TextBoxUfoNmb.Text);
                mapSizeNmb = int.Parse(this.TextBoxMapSize.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("A bemeneti paraméterek nem megfelelőek!", "Hiba!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            
            Conductor.Init(iterateNmb, soldierNmb, ufoNmb, mapSizeNmb);

            Stopwatch sw = new Stopwatch();

            if (this.ComboBoxMode.SelectedItem == null)
            {
                MessageBox.Show("Kérem válasszon ki egy szimulációs módot!");
            }
            else if ((Mode)this.ComboBoxMode.SelectedItem == Mode.Szekvenciális)
            {
                sw.Reset();
                sw.Start();
                Conductor.StartSimulationSequence();
                sw.Stop();

                MessageBox.Show("A szekvenciális futásidő: " + sw.Elapsed + "\nAz életben maradt katonák száma: " + Conductor.LiveSoldiers + 
                    "\n Az életben maradt ufók száma: " + Conductor.LiveUfos);

            }
            else
            {
                sw.Reset();
                sw.Start();
                Conductor.StartSimulationParallel();

                sw.Stop();

                MessageBox.Show("A párhuzamos futásidő: " + sw.Elapsed + "\nAz életben maradt katonák száma: " + Conductor.LiveSoldiers +
                    "\nAz életben maradt ufók száma: " + Conductor.LiveUfos);
            }
        }
    }

    public enum Mode
    {
        Szekvenciális,
        Párhuzamos
    }

}
