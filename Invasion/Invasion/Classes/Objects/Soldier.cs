﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invasion.Classes.Objects
{
    public class Soldier : BaseClass
    {
        public int LastLunch { get; set; }

        public override void Move(BaseClass[,] map, BaseClass enemy)
        {
            if (!IsAlive)
            {
                Iter++;
                return;
            }

            var ufo = enemy as Ufo;

            if (ufo != null && ufo.IsAlive)
            {
                ufo.Target(this);
                LastLunch = 0;
            }

            //Megöltük az ufot, a térképen átmozgatjuk magunkat a helyére
            map[enemy.Coordinate.X, enemy.Coordinate.Y] = this;
            map[this.Coordinate.X, this.Coordinate.Y] = enemy;

            //A tárolt koordintát is átvesszük. (Az ufo objektum már halott..)
            this.PreviousCoordinate = this.Coordinate;
            enemy.PreviousCoordinate = enemy.Coordinate;

            this.Coordinate = enemy.Coordinate;
            enemy.Coordinate = this.PreviousCoordinate;

            Iter++;

            LastLunch++;

            if (LastLunch > 300)
            {
                IsAlive = false;
            }
        }

        public override BaseClass NextPoint(BaseClass[,] map)
        {
            var seg = GetEnvironment(map).Where(x => x is Ufo && x.IsAlive);

            if (seg.Count() > 0)
            {
                Ufo enemy = seg.First() as Ufo;

                //if (enemy.IsAlive)
                //{
                //    return enemy;
                //}
                //else
                //{
                //    //utánamegy....(mehetne ha tudna... :D)
                //    return this.RandomMove(map);
                //}

                return enemy;
            }
            else
            {
                return this.RandomMove(map);
            }
        }
    }
}
