﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invasion.Classes.Objects
{
    /// <summary>
    /// A pályát alkotó üres mezőket szimbolizálja.
    /// </summary>
    public class NullObject : BaseClass
    {
        public NullObject()
        {
            IsAlive = false;
        }

        public override void Move(BaseClass[,] map, BaseClass next)
        {
            throw new NotImplementedException();
        }

        public override BaseClass NextPoint(BaseClass[,] map)
        {
            throw new NotImplementedException();
        }
    }
}
