﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invasion.Classes.Objects
{
    public class Ufo : BaseClass
    {
        /// <summary>
        /// Egy egész szám, ezzel elosztva a ciklusváltozót kapjuk meg, hogy osztódik-e.
        /// </summary>
        public int FissionTime { get; set; }

        public override void Move(BaseClass[,] map, BaseClass Next)
        {
            if (IsAlive)
            {
                this.MoveNextPoint(Next.Coordinate, map);
                this.Iter++;
                FissionTime++;

                if (FissionTime > 200)
                {
                    this.Fission(map);
                    FissionTime = 0;
                }
            }
            else
            {
                Iter++;
            }

        }

        private void Fission(BaseClass[,] map)
        {
            var newUfo = new Ufo() { Iter = this.Iter };
            newUfo.Coordinate = this.NextPoint(map).Coordinate;

            map[newUfo.Coordinate.X, newUfo.Coordinate.Y] = newUfo;
        }

        public override BaseClass NextPoint(BaseClass[,] map)
        {
            return this.RandomMove(map);
        }

        public bool Target(Soldier soldier)
        {
            this.IsAlive = false;
            return true;
        }
    }
}
