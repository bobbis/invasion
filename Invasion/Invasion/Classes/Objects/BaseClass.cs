﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invasion.Classes.Objects
{
    //System Drawing tudja ugyanezt...
    public class Point : IEquatable<Point>
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Point() { }

        public Point(int x, int y)
        {
            this.X = x;
            this.Y = y;

        }

        public bool Equals(Point other)
        {
            if (this.X == other.X && this.Y == other.Y)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    /// <summary>
    /// Alap osztály.
    /// </summary>
    public abstract class BaseClass
    {
        Random _rnd = new Random();

        public int Id { get; set; }

        public int Iter { get; set; }

        public Point Coordinate { get; set; }

        public Point PreviousCoordinate { get; set; }
        public BaseClass PrevPoint { get; internal set; }
        public bool IsAlive { get; set; }

        public int LastUpdateTime { get; set; }

        public int Age { get; set; }


        public BaseClass()
        {
            this.IsAlive = true;
        }

        /// <summary>
        /// Visszaadja a közvetlen környezetében lévő objektumokat (ami nem null)
        /// </summary>
        public List<BaseClass> GetEnvironment(BaseClass[,] map)
        {
            List<BaseClass> result = new List<BaseClass>();

            bool XMinusOk = this.Coordinate.X > 0;
            bool YMinusOk = this.Coordinate.Y > 0;
            bool XPlusOK = this.Coordinate.X + 1 < map.GetLength(0);
            bool YPlusOK = this.Coordinate.Y + 1 < map.GetLength(1);

            if (XMinusOk)
            {
                result.Add(map[this.Coordinate.X - 1, this.Coordinate.Y]);
            }

            if (XPlusOK)
            {
                result.Add(map[this.Coordinate.X + 1, this.Coordinate.Y]);
            }

            if (YMinusOk)
            {
                result.Add(map[this.Coordinate.X, this.Coordinate.Y - 1]);
            }

            if (YPlusOK)
            {
                result.Add(map[this.Coordinate.X, this.Coordinate.Y + 1]);
            }

            if (XMinusOk && YMinusOk)
            {
                result.Add(map[this.Coordinate.X - 1, this.Coordinate.Y - 1]);
            }

            if (XMinusOk && YPlusOK)
            {
                result.Add(map[this.Coordinate.X - 1, this.Coordinate.Y + 1]);
            }

            if (XPlusOK && YMinusOk)
            {
                result.Add(map[this.Coordinate.X + 1, this.Coordinate.Y - 1]);
            }

            if (XPlusOK && YPlusOK)
            {
                result.Add(map[this.Coordinate.X + 1, this.Coordinate.Y + 1]);
            }

            return result.Where(x => x != null).ToList();
        }

        /// <summary>
        /// Random Koordináta nem mozog
        /// </summary>
        public BaseClass RandomMove(BaseClass[,] map)
        {
            List<Point> PossibleCoordinates = new List<Point>();

            bool XMinusOk = this.Coordinate.X > 0;
            bool YMinusOk = this.Coordinate.Y > 0;
            bool XPlusOK = this.Coordinate.X + 1 < map.GetLength(0) - 1;
            bool YPlusOK = this.Coordinate.Y + 1 < map.GetLength(1) - 1;


            if (XMinusOk && map[this.Coordinate.X - 1, this.Coordinate.Y].IsAlive == false)
            {
                PossibleCoordinates.Add(new Point(this.Coordinate.X - 1, this.Coordinate.Y));
            }

            if (XPlusOK && map[this.Coordinate.X + 1, this.Coordinate.Y].IsAlive == false)
            {
                PossibleCoordinates.Add(new Point(this.Coordinate.X + 1, this.Coordinate.Y));
            }

            if (YMinusOk && map[this.Coordinate.X, this.Coordinate.Y - 1].IsAlive == false)
            {
                PossibleCoordinates.Add(new Point(this.Coordinate.X, this.Coordinate.Y - 1));
            }

            if (YPlusOK && map[this.Coordinate.X, this.Coordinate.Y + 1].IsAlive == false)
            {
                PossibleCoordinates.Add(new Point(this.Coordinate.X, this.Coordinate.Y + 1));
            }

            if (XMinusOk && YMinusOk && map[this.Coordinate.X - 1, this.Coordinate.Y - 1].IsAlive == false)
            {
                PossibleCoordinates.Add(new Point(this.Coordinate.X - 1, this.Coordinate.Y - 1));
            }

            if (XMinusOk && YPlusOK && map[this.Coordinate.X - 1, this.Coordinate.Y + 1].IsAlive == false)
            {
                PossibleCoordinates.Add(new Point(this.Coordinate.X - 1, this.Coordinate.Y + 1));
            }

            if (XPlusOK && YMinusOk && map[this.Coordinate.X + 1, this.Coordinate.Y - 1].IsAlive == false)
            {
                PossibleCoordinates.Add(new Point(this.Coordinate.X + 1, this.Coordinate.Y - 1));
            }

            if (XPlusOK && YPlusOK && map[this.Coordinate.X + 1, this.Coordinate.Y + 1].IsAlive == false)
            {
                PossibleCoordinates.Add(new Point(this.Coordinate.X + 1, this.Coordinate.Y + 1));
            }

            //TODO: Nagyon este van, ez így elég kaki...
            if (PossibleCoordinates.Where(x => x == this.PreviousCoordinate).Count() > 0)
            {
                PossibleCoordinates.Remove(PossibleCoordinates.Where(x => x == this.PreviousCoordinate).First());
            }

            if (PossibleCoordinates.Count > 0)
            {
                Point randomPoint = PossibleCoordinates[_rnd.Next(0, PossibleCoordinates.Count)];

                return map[randomPoint.X, randomPoint.Y];
            }
            else
            {
                //ha nem tud hova lépni, lépjen önmagába..
                return this;
            }
        }

        public void MoveNextPoint(Point randomPoint, BaseClass[,] map)
        {
            var prev = map[randomPoint.X, randomPoint.Y];
            map[randomPoint.X, randomPoint.Y] = this;
            map[this.Coordinate.X, this.Coordinate.Y] = prev;

            this.PreviousCoordinate = this.Coordinate;
            prev.PreviousCoordinate = prev.Coordinate;

            this.Coordinate = prev.Coordinate;
            prev.Coordinate = this.PreviousCoordinate;
        }

        public abstract void Move(BaseClass[,] map, BaseClass next);

        public abstract BaseClass NextPoint(BaseClass[,] map);
    }
}

