﻿using Invasion.Classes.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Invasion.Classes.Simulation
{
    public static class Conductor
    {
        static int _taskId = 0;
        static int _objectId = 0;
        static int _taskNmb;
        static BaseClass[,] _map;
        static Random _rnd = new Random();

        static int _iterateNmb;
        static int _soldierNmb;
        static int _ufoNmb;
        static int _mapSizeNmb;

        /// <summary>
        /// Futás után életben maradt ufok száma
        /// </summary>
        public static int LiveUfos { get; set; }

        /// <summary>
        /// Futás után életben maradt katonák száma
        /// </summary>
        public static int LiveSoldiers { get; set; }
        
        #region Initialize

        public static void Init(int iterateNmb, int soldierNmb, int ufoNmb, int mapSizeNmb)
        {
            _iterateNmb = iterateNmb;
            _soldierNmb = soldierNmb;
            _ufoNmb = ufoNmb;
            _mapSizeNmb = mapSizeNmb;

            _taskId = 0;
            _objectId = 0;
            LiveSoldiers = 0;
            LiveUfos = 0;

            CreateMap(mapSizeNmb);
        }

        private static void CreateMap(int size)
        {
            _map = new BaseClass[size, size];

            UfoFactory(_ufoNmb);            //Létrehozza az ufo objektumokat
            SoldierFactory(_soldierNmb);    //Létrehozza a katona objektumokat
            NullObjectFactory();            //Feltölti a null helyeket NullObject-ekkel
        }

        private static void NullObjectFactory()
        {
            for (int i = 0; i < _map.GetLength(0); i++)
            {
                for (int j = 0; j < _map.GetLength(1); j++)
                {
                    if (_map[i, j] == null)
                    {
                        _map[i, j] = new NullObject();

                        _map[i, j].Coordinate = new Point(i, j);
                    }
                }
            }
        }

        static void UfoFactory(int db)
        {
            for (int i = 0; i < db; i++)
            {
                Ufo add = new Ufo()
                {
                    Id = _objectId++,
                    Coordinate = RandomCoordinate(_mapSizeNmb)
                };

                _map[add.Coordinate.X, add.Coordinate.Y] = add;
            }
        }

        static void SoldierFactory(int db)
        {
            for (int i = 0; i < db; i++)
            {
                Soldier add = new Soldier()
                {
                    Id = _objectId++,
                    Coordinate = RandomCoordinate(_mapSizeNmb)
                };

                _map[add.Coordinate.X, add.Coordinate.Y] = add;
            }

        }

        static Point RandomCoordinate(int max)
        {
            Point res;
            do
            {
                res = new Point(_rnd.Next(0, max), _rnd.Next(0, max));

            } while (_map[res.X, res.Y] != null);

            return res;
        }

        #endregion Initialize

        public static void StartSimulationSequence()
        {
            int db = 0;
            while (db < _iterateNmb)
            {
                foreach (BaseClass i in _map)
                {
                    if (!(i is NullObject))
                    {
                        i.Move(_map, i.NextPoint(_map));
                    }
                }
                db++;
            }

            foreach (BaseClass i in _map)
            {
                if (i is Ufo ufo && ufo.IsAlive)
                {
                    LiveUfos++;
                }

                if (i is Soldier soldier && soldier.IsAlive)
                {
                    LiveSoldiers++;
                }
            }
        }

        public static void StartSimulationParallel()
        {
            _taskNmb = Environment.ProcessorCount;

            Task[] tasks = StartTasks(_taskNmb);

            Task.WaitAll(tasks);

            foreach (BaseClass i in _map)
            {
                if (i is Ufo ufo && ufo.IsAlive)
                {
                    LiveUfos++;
                }

                if (i is Soldier soldier && soldier.IsAlive)
                {
                    LiveSoldiers++;
                }
            }

        }

        static Task[] StartTasks(int taskNmb)
        {
            List<Task> result = new List<Task>();


            //Elindít taskNmb-db taskot melyek a mátrix meghatározott részein végzik a szimulációt.
            //Az elosztás dinamikusan, sorokra bontva a taskok darabszámától függ.
            for (int i = 0; i < taskNmb; i++)
            {
                int locID = _taskId;
                int maxTaskId = _taskNmb - 1;
                int rowNmb = _map.GetLength(1) / _taskNmb;

                int startRow = locID * rowNmb;
                int ednRow = locID == maxTaskId ? _map.GetLength(1) : (startRow + rowNmb);

                _taskId++;

                result.Add(Task.Factory.StartNew(() => TaskFunction(startRow, ednRow), TaskCreationOptions.LongRunning));
            }

            return result.ToArray();
        }

        static void TaskFunction(int startRowIndex, int endRowIndex)
        {
            int db = 1;

            while (db < _iterateNmb)
            {
                for (int j = startRowIndex; j < endRowIndex; j++)
                {
                    for (int i = 0; i < _map.GetLength(0); i++)
                    {
                        var element = _map[i, j];

                        if (!(element is NullObject))
                        {
                            while (element.Iter < db)
                            {
                                BaseClass next = null;
                                if (Monitor.TryEnter(element))
                                {
                                    next = element.NextPoint(_map);

                                    if (next != null && Monitor.TryEnter(next))
                                    {

                                        element.Move(_map, next);

                                        Monitor.Exit(element);
                                        Monitor.Exit(next);
                                    }
                                    else //if (Monitor.IsEntered(element))
                                    {
                                        Monitor.Exit(element);
                                    }
                                }
                            }
                        }
                    }
                }

                db++;
            }
        }

    }

}
